import uuid
from datetime import datetime

from django.db import models
from jsonfield import JSONField

# Create your models here.


class Project(models.Model):
    """ Project model """

    name = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now=True)
    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    """ Schedule model """

    name = models.CharField(max_length=100)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True,
                                related_name="schedules")
    status = models.CharField(max_length=50, default="New")
    schedule_file = models.FileField(upload_to='schedule_files/', default=None)
    deadline = models.DateTimeField(default=None, null=True)


class Calendar(models.Model):
    """ Calendar model """

    name = models.CharField(max_length=100)
    calendar_id = models.CharField(max_length=20, primary_key=True)

    # The working hours of the calendar in a json field.
    # Since we wont be updating this often, it is okay to store it as JSON.
    # Beside, this enables us to easily fetch the working hours as a python dict.
    #
    # An example for working_hours is:
    # {
    #     "1": [],
    #     "2": [["09:00:00", "12:00:00"], ["13:00:00", "17:00:00"]],
    #     "3": [["08:00:00", "12:00:00"], ["13:00:00", "18:30:00"]],
    #     "4": [["08:00:00", "12:00:00"], ["13:00:00", "17:00:00"]],
    #     "5": [["10:00:00", "12:00:00"], ["12:30:00", "19:00:00"]],
    #     "6": [["08:00:00", "12:00:00"], ["13:00:00", "17:00:00"]],
    #     "7": []
    # }
    working_hours = JSONField()

    def __str__(self):
        return self.name


class Task(models.Model):
    """ Task model """
    task_id = models.CharField(max_length=20, primary_key=True)

    name = models.CharField(max_length=200)

    start = models.DateTimeField(default=None, blank=True, null=True)
    # The date the task ended.
    # This can be null (Eg, task has not been completed yet)
    end = models.DateTimeField(default=None, blank=True, null=True)

    # tasks have a calendar which indicates the start and end times
    # in each day of the week
    calendar = models.ForeignKey(
        Calendar, on_delete=models.DO_NOTHING, default=None, null=True,
        related_name="tasks"
    )
    # Total number of hours spent on task.
    # This will be computed and saved lateR, perhaps by a celery task
    num_working_hours = models.IntegerField(
        default=None, blank=True, null=True)

    # a task may be associated to a specific schedule
    schedule = models.ForeignKey(
        Schedule, on_delete=models.CASCADE, default=None, blank=True, null=True,
        related_name='tasks'
    )
