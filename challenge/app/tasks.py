""" Collection of functions for computing/updating the working hours of tasks """
from datetime import date, datetime, time, timedelta
from typing import Generator, Union

from celery.task import task

from challenge.app.models import Task

START_OF_A_DAY = time(0, 0, 0, 0)
END_OF_A_DAY = time(23, 59, 59, 999999)


@task(name="update_tasks_working_hours")
def update_tasks_working_hours():
    """ Compute the working hours for tasks and update them in the database """
    tasks = Task.objects.all()
    for task_ in tasks:
        task_.num_working_hours = compute_task_working_hours(task)

    Task.objects.bulk_create(tasks)  # save the tasks to the database


def compute_task_working_hours(task: Task) -> Union[int, None]:
    """ Compute the working hours for a given task """

    # If there is no end-date,
    # assume that the task has not ended yet and is ongoing
    if not task.end:
        task.end = datetime.now()

    if not task.start or task.start > task.end:  # <-- Invalid data input
        return None

    num_working_hours = 0.0

    next_day = task.start.date() + timedelta(days=1)  # next day after start day
    # for each full day after the start day, compute working hours for it
    for day in date_range(next_day, task.end.date()):  # loop till last but one day
        num_working_hours += __compute_hours_in_day(
            task.calendar,
            start_time=START_OF_A_DAY,  # start of day
            end_time=END_OF_A_DAY,  # end of day
            day_of_week=get_weekday(day)
        )

    # Now, compute the working hours in the start and end days
    if task.start.date() == task.end.date():  # <-- EDGE CASE
        # The task starts and ends in the same day or at the same time
        num_working_hours += __compute_hours_in_day(
            task.calendar, task.start.time(), task.end.time(), get_weekday(task.end))
    else:
        # compute the hours in the starting day
        num_working_hours += __compute_hours_in_day(
            task.calendar, task.start.time(), END_OF_A_DAY, get_weekday(task.start))
        # compute the hours in the ending day
        num_working_hours += __compute_hours_in_day(
            task.calendar, START_OF_A_DAY, task.end.time(), get_weekday(task.end))

    return int(num_working_hours)  # round down


def __compute_hours_in_day(
        calendar: dict, start_time: time, end_time: time, day_of_week: int) -> int:
    num_working_hours = 0.0

    if start_time == end_time:  # <-- EDGE CASE
        return num_working_hours

    for cal_start_, cal_end_ in calendar.working_hours[str(day_of_week)]:
        cal_start = datetime.strptime(cal_start_, '%H:%M:%S').time()
        cal_end = datetime.strptime(cal_end_, '%H:%M:%S').time()

        # make sure the task does not starts after or end before the calendar time span
        if start_time > cal_end or end_time < cal_start:
            continue

        actual_start = cal_start if cal_start > start_time else start_time
        actual_end = cal_end if cal_end < end_time else end_time
        # convert to timedelta for mathematical manipulation
        actual_start = timedelta(
            hours=actual_start.hour, minutes=actual_start.minute, seconds=actual_start.second)
        actual_end = timedelta(
            hours=actual_end.hour, minutes=actual_end.minute, seconds=actual_end.second)

        diff = actual_end - actual_start

        num_working_hours += diff.seconds/3600.0  # convert seconds to hours

    return num_working_hours


def date_range(start: date, stop: date, step: int = 1) -> Generator:
    """ A version of range() for dates.

    Starts from 'start' and yields the subsequent days after 'start' until 'end' - 1 day,
    separated by 'step'.

    Returns:
        Generator -- A generator that yields the subsequent days
    """
    days_diff = (stop - start).days
    for i in range(days_diff):
        yield start + timedelta(days=i)


def get_weekday(date_: Union[datetime, date]) -> int:
    """ Get the day of the week of a date as a number.

    Returns:
        int -- Such that Monday=1, Tuesday=2, ..., Sunday=7
    """
    # Python returns date.weekday() --> (Mon-Sun, 0-6), but, we expect (Mon-Sun, 1-7).
    return date_.weekday() + 1  # so add 1
