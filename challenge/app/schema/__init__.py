""" Graphql scheme definitions for this app's API """
from .mutation import Mutation
from .query import Query
