""" Mutations that can be performed on this app's API """

__all__ = ['Mutation']


class Mutation(object):
    """
    Definition for mutations that can be performed on
    this app's API
    """
    # todo: Any mutations goes here
