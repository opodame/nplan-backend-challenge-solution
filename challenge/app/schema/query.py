""" Definition of queries that can be performed on this app's API"""
import graphene

import challenge.app.models as db_model

from .types import TaskType, TaskWorkingHours
from challenge.app.tasks import compute_task_working_hours


class Query(object):
    """Definition for queries that can be performed on this app's API"""
    get_tasks = graphene.List(
        TaskType,
        schedule_id=graphene.ID(),
        first=graphene.Int(), offset=graphene.Int(), search=graphene.String(),
        filter=graphene.String(), sort_by=graphene.String(),
        required=True
    )

    get_tasks_working_hours = graphene.List(
        TaskWorkingHours,
        tasks_ids=graphene.List(graphene.ID, required=True),
        required=True
    )

    def resolve_get_tasks(self, info, schedule_id):
        """ Returns all tasks for a given {schedule_id} """
        try:
            schedule = db_model.Schedule.objects.get(id=schedule_id)
        except:
            return []

        task_list = db_model.Task.objects.filter(schedule=schedule)
        # .order_by(*json.loads(sort_by)) # todo: retake a look at sorting later
        return task_list

    def resolve_get_tasks_working_hours(self, info, tasks_ids):
        """ Returns the number of working hours of a given set of tasks """
        tasks = db_model.Task.objects.filter(task_id__in=tasks_ids)

        return [
            TaskWorkingHours(
                task_id=task.task_id,

                # NB: ideally, the hours would have been already computed and saved in the
                # database
                hours=compute_task_working_hours(task)
            )
            for task in tasks
        ]