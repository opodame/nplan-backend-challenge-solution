"""Type definitions used in this app"""

import graphene
from graphene_django import DjangoObjectType
import challenge.app.models as db_model

# these are the public names
__all__ = [
    'ProjectType', 'ScheduleType', 'TaskType', 'TaskWorkingHours',
]


class ProjectType(DjangoObjectType):
    """ Django object type for returning Projects to the user """

    class Meta:
        """ Uses the Project model defined in models.py """
        model = db_model.Project


class ScheduleType(DjangoObjectType):
    """ Django object type for returning Schedules to the user """

    class Meta:
        """ Uses the Schedule model defined in models.py """
        model = db_model.Schedule


class TaskType(DjangoObjectType):
    """ Django object type for returning Tasks to the user """

    class Meta:
        """ Uses the Task model defined in models.py """
        model = db_model.Task


# NB: We dont explicitly return Calendar entities to the user,
#   so we dont have to create a resolver for it.


class TaskWorkingHours(graphene.ObjectType):
    """ Graphql type for returning a task's working hours """
    task_id = graphene.String(required=True)
    # can be null, if it has not been computed yet
    hours = graphene.Float(required=False)
