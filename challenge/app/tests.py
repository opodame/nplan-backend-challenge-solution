from datetime import datetime

import pytest
from django.utils import timezone
from graphene.test import Client

from challenge.app.models import Calendar, Task
from challenge.app.tasks import compute_task_working_hours
from challenge.schema import schema


def get_dummy_calendar():
    return Calendar(
        name='Calendar',
        calendar_id='1',
        working_hours={
            "1": [],
            "2": [["09:00:00", "12:00:00"], ["13:00:00", "17:00:00"]],
            "3": [["08:00:00", "12:00:00"], ["13:00:00", "18:30:00"]],
            "4": [["08:00:00", "12:00:00"], ["13:00:00", "17:00:00"]],
            "5": [["10:00:00", "12:00:00"], ["12:30:00", "19:00:00"]],
            "6": [["08:00:00", "12:00:00"], ["13:00:00", "17:00:00"]],
            "7": []
        }
    )


class TestComputeWorkingHours(object):

    def test_compute_working_hours(self):
        task = Task(
            name="Task",
            start=datetime(2019, 2, 11, 10, 0, 0, tzinfo=timezone.utc),
            end=datetime(2019, 2, 12, 13, 0, 0, tzinfo=timezone.utc),
            calendar=get_dummy_calendar()
        )
        num_working_hours = compute_task_working_hours(task)
        assert num_working_hours == 3

    def test_compute_working_hours__start_and_end_same_time(self):
        """ Test for the scenario where task starts and ends at
        the exact same time"""
        task = Task(
            name="Task",
            start=datetime(2019, 2, 12, 13, 0, 0, tzinfo=timezone.utc),
            end=datetime(2019, 2, 12, 13, 0, 0, tzinfo=timezone.utc),
            calendar=get_dummy_calendar()
        )
        num_working_hours = compute_task_working_hours(task)
        assert num_working_hours == 0

    def test_comput_working_hours__start_and_end_same_date(self):
        task = Task(
            name="Task",
            start=datetime(2019, 2, 11, 15, 0, tzinfo=timezone.utc),
            end=datetime(2019, 2, 11, 15, 0, tzinfo=timezone.utc),
            calendar=get_dummy_calendar()
        )
        num_working_hours = compute_task_working_hours(task)
        assert num_working_hours == 0

        task.start = datetime(2019, 2, 12, 15, 0, 0, tzinfo=timezone.utc)
        task.end = datetime(2019, 2, 12, 15, 0, 0, tzinfo=timezone.utc)
        num_working_hours = compute_task_working_hours(task)
        assert num_working_hours == 0

    def test_compute_working_hours__null_start(self):
        """ Test for the scenario where start is greater than end.
        Perhaps, our ML algorithm developed a bug :)
        """
        task = Task(
            name="Task",
            start=datetime(2019, 2, 12, 23, 0, 0, tzinfo=timezone.utc),
            end=datetime(2019, 2, 12, 15, 0, 0, tzinfo=timezone.utc),
            calendar=get_dummy_calendar()
        )
        num_working_hours = compute_task_working_hours(task)
        assert num_working_hours == None

    def test_compute_working_hours__start_and_end_same_date___no_overlaps(self):
        task = Task(
            name="Task",
            start=datetime(2019, 2, 11, 5, 0, 0, tzinfo=timezone.utc),
            end=datetime(2019, 2, 12, 8, 42, 0, tzinfo=timezone.utc),
            calendar=get_dummy_calendar()
        )
        num_working_hours = compute_task_working_hours(task)
        assert num_working_hours == 0


@pytest.mark.django_db  # tell pytest that we need access to the database
def test_query_tasks_working_hours():
    # fill the database with some task for the test
    task = Task(
        name="Task",
        start=datetime(2019, 2, 11, 10, 0, 0, tzinfo=timezone.utc),
        end=datetime(2019, 2, 12, 13, 0, 0, tzinfo=timezone.utc),
        calendar=get_dummy_calendar(),
        task_id='1'
    )
    task.save()

    client = Client(schema=schema)
    result = client.execute(
        """
        query {
            getTasksWorkingHours(tasksIds: [0, 1]) {
                taskId
                hours
            }
        }
        """
    )
    assert 'data' in result
    assert result['data'].get('getTasksWorkingHours')

    first_task_hour = result['data']['getTasksWorkingHours'][0]
    assert first_task_hour['taskId'] == "0"
    assert first_task_hour['hours'] == 474
