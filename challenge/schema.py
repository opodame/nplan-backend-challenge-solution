
"""
The overall Schema for the project's API.
Here, we combine the definitions from the various app(s) in the project
"""
import graphene
from challenge.app.schema import Query as ChallengeAppQuery


class Query(
        ChallengeAppQuery,  # include the schema from the 'Challenge' app
        # ...QueryFromAnotherApp
        # as we add more apps to the project, we will include their here
        graphene.ObjectType
):
    """
    Defines all the queries that can be made to the API,
    based on the Query definitions from the various apps in the project
    """


class Mutation(
        # ...SomeAppMutations,
        # ...AnotherAppMutations.
        graphene.ObjectType
):
    """
    Defines all the mutations that can be made with this API,
    base on the Mutation definitions from various apps in the project
    """


schema = graphene.Schema(
    query=Query,
    # mutation=Mutation
)
