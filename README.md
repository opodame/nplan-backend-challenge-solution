# Solution to Challenge

## **Project Structure**
```
    challenge/
        app/
            __init__.py
            migrations/
                __init__.py
                ...migration files
            schema/
                __init__.py
                mutation.py
                query.py
                types.py
            apps.py
            models.py
            tasks.py
            tests.py
        celery.py
        schema.py
        settings.py
        test_settings.py
        urls.py
        .gitignore
        backend-challenge.sqlite
        manage.py
        pytest.ini
        README.md
        requirements.txt
        test_requirements.txt
```

The name of the django project is ```challenge``` and there is a single app called ```app``` inside it as a python package.

```app``` houses a package called ```schema```, which contains graphql definitions for *query*, *types* and *mutations*. These deinitions are imported in project-level schema at ```challenge.schema```. This approach, promotes the idea of separation of concerns.

```test_settings.py``` contains settings to be used when we are running tests. A different database (in-memory sqlite) is used to avoid having to pollute the actual database with random data.



## **Get-Tasks-Working-Hours Algorithm**

#### Assumptions
- If the *start* of a task is null, then its working hours is 0. The task is considered to be *yet-to-be-started*.
- For a given task, if the *start* is later than the *end*, the working hours is null. Ideally, this scenario should not occur.

#### Approach

```py
    tasks = [id1, id2, ...] 
    fetch_tasks_objects_from_db()
    
    iterate over each task and perform the following

    Given a task,
    If task.end_date is null, assume it is current time
    
    If task.start_date is null or task only starts after it has ended
        treat as an invalid input
        return null
    
    Initially num_working_hours is set to 0


    For each day after task.start_date, but before task.end_date,

        hours = compute working hours based on the calendar for the start-TIME and end-TIME of day

        num_working_hours += hours
    

    If task starts and ends in same day,

        compute working hours based on calendar for task start-TIME and task end-TIME

    Else,

        compute working hours based on calendar for task start-TIME and 23:59:59.9999999 (the last tick in a day)

        compute working hours based on calendar for 0:0:0.00000 (the first tick of the day) and task end-TIME




    # *****************************************************************************
    # compute working hours based on calendar for start-TIME and end-TIME of a day
    # *****************************************************************************
    There are multiple start/stops (breaks) in a day, so we copute hours for each pair of start and stop in the day

    To compute for a single start-stop pair, we do the following
    
    Check that the tasks does not start after this start-stop pair
    Also check that the task does not end before the start-stop pair.
    If any of these is True, we return ZERO for that start-stop pair

    Find the portion of the two time spans that overlaps and divide into hours
    This is the number of hours between the start-stop pair and also the task start-TIME and end-TIME
    # *****************************************************************************
```

## **Incorporating Celery**
Celery allows asynchronous behaviour to be achieved in python.
With celery we can have tasks run in the background(outside of the request context).

For the use case at hand, where a machine learning model runs millions of computations before returning the working hours,
we will set up a celery task that will be invoked, once the results from the ML model is ready.
This celery task will compute the tasks working hours based on the information provided by the ML model and save the results in the database.
Until the celery task has been invoked, querying for the working hours should just return null.

----

With this, approach, the procedure for computing the working hours can further be optimized.

After the very first computation has been done for the working hours of a task, we can keep track of the number of working hours and the date up to which it was computed. 

This way, subsequent celery task invocations will have to deal with smaller date ranges.



## **Running the app**
You must have all the required dependencies installed. Preferably you will want to do this in a virtual environment.

1. Clone this repo

        git clone https://opodame@bitbucket.org/opodame/nplan-backend-challenge-solution.git

2. Change directory to the project root folder

        cd nplan-backend-challenge-solution

3. Create and activate virtual environment

        virtualenv .venv && source ./.venv/bin/activate

4. Install dependencies

        pip install -r requirements.txt

5. Run migrations to create sqlite db and seed db with data

        python manage.py migrate

6. Run django project

        python manage.py runserver

The GraphQL API should be up and running on http://localhost:8000/graphql


## **Testing**
This project uses ```pytest``` (with the ```pytest-django``` plugin) for tests.

You must have these installed, in order to run tests.

    pip install -r test_requirements.txt

With  ```pytest``` installed, just run in your terminal

    pytest

## **TODO**
Restructure the contents of *challenge/app/fixtures/date-calendar-seed.json* in order to support loading the data with the ```python manage.py loaddata``` command

